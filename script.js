// local time now 
let time = document.getElementById("txt");
setInterval(() => {
    let d = new Date();
    time.innerHTML = d.toDateString() + " " + d.toLocaleTimeString();;
}, 1000);

function button() {
    var status = document.getElementById("button");
    // start btn
    if (status.value == "Start") {
        currenttime = new Date();
        st_hour = currenttime.getHours();
        st_min = currenttime.getMinutes();
        document.getElementById("starts").innerHTML = st_hour + ":" + st_min;
        document.getElementById("button").value = "stop";
        document.getElementById("button").innerHTML = '<div><i class="fa-solid fa-circle-stop"></i> Stop</div>';
        document.getElementById("button").style.backgroundColor = 'red';
    }
    // stop btn
    else if (status.value == "stop") {

        document.getElementById("button").innerHTML = '<div> <i class="fa-solid fa-trash-can"></i> Clear</div>';
        document.getElementById("button").value = "clear";
        document.getElementById("button").style.backgroundColor = '#FFB100';
        var endtime = new Date();
        end_hour = endtime.getHours();
        end_minute = endtime.getMinutes();
        document.getElementById("end").innerHTML = end_hour + ":" + end_minute;
        hour = end_hour - st_hour;
        minute = hour * 60;
        remain_minute = end_minute - st_min;
        total_in_minute = remain_minute + minute;
        // var total_in_Minute = 80;
        document.getElementById("minute").innerHTML = total_in_Minute;

        if (total_in_Minute <= 15) {
            document.getElementById("riel").innerHTML = "500";
        } else if (total_in_Minute <= 30) {
            document.getElementById("riel").innerHTML = "1000";
        } else if (total_in_Minute <= 60) {
            document.getElementById("riel").innerHTML = "1500";
        } else {
            if (total_in_Minute <= 15) {
                document.getElementById("riel").innerHTML = "500";
            } else if (total_in_Minute <= 30) {
                document.getElementById("riel").innerHTML = "1000";
            } else if (total_in_Minute <= 60) {
                document.getElementById("riel").innerHTML = "1500";
            } else if (total_in_Minute > 60) {
                const overPriceFloat = Math.trunc(total_in_Minute % 60);
                const overPriceInt = Math.trunc(total_in_Minute / 60);
                if (overPriceFloat > 0) {
                    if (overPriceFloat > 0 && overPriceFloat <= 15) {
                        document.getElementById("riel").innerHTML = overPriceInt * 1500 + 500;
                    } else if (overPriceFloat > 15 && overPriceFloat <= 30) {
                        document.getElementById("riel").innerHTML = overPriceInt * 1500 + 1000;
                    } else if (overPriceFloat > 30 && overPriceFloat <= 60) {
                        document.getElementById("riel").innerHTML = overPriceInt * 1500 + 1500;
                    }
                }
                else {
                    document.getElementById("riel").innerHTML = overPriceInt * 1500;
                }

            }
        }
    } else {
        document.getElementById("starts").innerHTML = "00:00";
        document.getElementById("end").innerHTML = "00:00";
        document.getElementById("minute").innerHTML = "00";
        document.getElementById("riel").innerHTML = "00";
        document.getElementById("button").value = "Start";
        document.getElementById("button").innerHTML = '<div> <i class="fa-solid fa-play"></i> Start</div>';
        document.getElementById("button").style.backgroundColor = 'green';
    }

}